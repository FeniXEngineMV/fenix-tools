/**
 * Checks if file(s) exist on a local machine using node fs.
 *
 * @function localFileExists
 * @since 1.0.0
 * @memberof module:File
 *
 * @param {string|array} paths - A path or array of paths to check.
 *
 * @return {boolean} True if the file path or paths exist
 * @example
 * import {localFileExists} from 'fenix-tools'
 *
 * localFileExists('./img/pictures/myPicture.png') // => Returns true or false
 *
 */
export default function localFileExists (paths = []) {
  paths = typeof paths === 'string' ? [paths] : paths
  const fs = require('fs')
  return paths.every((path) => fs.existsSync(path))
}
