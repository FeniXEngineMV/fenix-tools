import localFileExists from './localFileExists'
import webFileExists from './webFileExists'

/**
 * Check if a file exists.
 *
 * @function fileExists
 * @since 1.0.0
 * @memberof module:File
 * @see {@link module:File.localFileExists|localFileExists}
 * @see {@link module:File.webFileExists|webFileExists}
 *
 * @param {string} path - The path to the file
 * @param {array} extensions -  the extension(s) of file you want to check
 *
 * @returns {Boolean} true if the file exists.
 * @example
 * import { fileExists } from 'fenix-tools'
 *
 * console.log(fileExists('./path/to/file')) // => returns true or false
 *
 */
export default function fileExists (url) {
  if (window.isNwjs()) {
    localFileExists(url)
  } else {
    webFileExists(url)
  }
}
