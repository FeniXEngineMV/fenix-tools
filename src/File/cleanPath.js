/**
 * Cleans the path by replacing two forward slashes with one.
 *
 * @function cleanPath
 * @since 1.0.0
 * @memberof module:File
 *
 * @param {string} path - The path you want to clean.
 * @return {string} The cleaned string.
 * @example
 * import { cleanPath } from 'fenix-tools'
 *
 * const badUrl = 'C://Path//to/something//'
 *
 * console.log(cleanPath(badUrl)) // => 'C:/Path/to/something/'
 */
export default function cleanPath (path) {
  return path.replace(/(\/)[(/)]/g, '/')
}
