/**
 * Retrieves meta values from an RPG Maker MV object that contains the meta property.
 * Will work with any object that contains a meta property, like $dataWeapons,
 * $dataItems, etc
 *
 * @function getMetaData
 * @since 1.0.0
 * @memberof module:Utils
 *
 * @param  {object} obj - The meta object you want to search through.
 * @param {string}  tag - The meta tag you want to search for.
 *
 * @returns {string} The value(s) of the notetag.
 * @example
 * import { getMetaData } from 'fenix-tools'
 *
 * // $dataActors[1].meta = {myTag: 'myTagValue'}
 *
 * const data = $dataActors[1]
 * const meta = getMetaData(data, 'myTag') // => 'myTagValue'
 *
 */
export default function getMetaData (obj, tag) {
  const meta = obj.meta
  const match = Object.keys(meta).filter((key) => key.toLowerCase() === tag.toLowerCase())
  const value = meta[match]
  return typeof value === 'string' ? value.trim() : value
}
