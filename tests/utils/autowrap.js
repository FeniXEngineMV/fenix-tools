import test from 'ava'
import autowrap from '../../src/Utils/autowrap'

test('autowrap - wrapping long lines', t => {
  const text = 'This is a long sentence that needs to be wrapped to fit within a certain width.'
  const wrappedLines = autowrap(text, 20)
  t.deepEqual(wrappedLines, [
    'This is a long',
    'sentence that needs',
    'to be wrapped to fit',
    'within a certain',
    'width.'
  ])
})

test('autowrap - ignoring RPG Maker codes', t => {
  const text = 'Hello my name is \\C[3]Ace\\C[0]'
  const wrappedLines = autowrap(text, 20, true)

  // This should fit all on one line with ignoreMessageCodes set to true
  t.deepEqual(wrappedLines, [
    'Hello my name is \\C[3]Ace\\C[0]'
  ])
})
