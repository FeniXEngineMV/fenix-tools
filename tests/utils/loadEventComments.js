import test from 'ava'
import { loadEventComments, loadJSON } from '../../src'

test('loadEventComments', async t => {
  // Assign mock to global $dataMap as this function requires MV's $dataMap global variable
  window.$dataMap = await loadJSON('./tests/helpers/Map001.json')
  const comments = loadEventComments()

  // Expected result taken directly from Json data
  const expectedResult = { 1: ['Comments !!!<multiTag>prop: value,cool: yes</multiTag><simpleTag: aString><anotherTag>prop: value,cool: yes</anotherTag>'] }
  t.deepEqual(comments, expectedResult, 'Must deep identical')
})
