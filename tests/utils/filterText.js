import test from 'ava'
import { filterText } from '../../src'

// filterText
test('filterText', t => {
  const re = /<([^<>]+)>([\s\S]*?)<(\/[^<>]+)>/g
  // Mock notetags
  const text = '<multiTag>prop: value, prop1: value2</multiTag>'
  // filterText returns each match as a group, its an array of arrays
  const tags = filterText(text, re, (match) => match[1] === 'multiTag')
  t.is(tags.length === 1, true, 'There should be 1 group, one for each tag matching the evaluation')
  const multiTag = tags[0]
  t.is(multiTag[1], 'multiTag')
})
