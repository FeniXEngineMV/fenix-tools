import test from 'ava'
import { convertParameters } from '../../src'

const testParams = {'General': '', 'keycode': '27', 'stopGameplay': 'true', 'text': 'Press Space to skip video', 'textX': '0', 'textY': '0', 'textStyle': '{"fontFamily":"Arial","fontSize":"36","fill":"[\\"#f74a4d\\",\\"#f79d4a\\"]"}'}

const expectedResult = {
  General: '',
  keycode: 27,
  stopGameplay: true,
  text: 'Press Space to skip video',
  textX: 0,
  textY: 0,
  textStyle: {
    fontFamily: 'Arial',
    fontSize: 36,
    fill: ['#f74a4d', '#f79d4a']
  }
}

test('convertParameters', t => {
  t.deepEqual(convertParameters(testParams), expectedResult)
})
