import test from 'ava'
import calculateTextWidth from '../../src/Utils/calculateTextWidth'

test('calculateTextWidth - without ignoring escape codes', t => {
  const text = 'Hello my name is \\C[3] Harold \\C[0].'
  const width = calculateTextWidth(text)
  t.is(width, 36)
})

test('calculateTextWidth - ignoring escape codes', t => {
  const text = 'Hello my name is \\C[3] Harold \\C[0].'
  const width = calculateTextWidth(text, true)
  t.is(width, 26)
})

test('calculateTextWidth - Calculates with message codes that add text', t => {
  const text = 'Hello my name is \\N[1]'
  const width = calculateTextWidth(text, true)
  t.is(width, 23)

  const text2 = 'Hello my name is \\P[1]'
  const width2 = calculateTextWidth(text2, true)
  t.is(width2, 23)

  const text3 = 'I am \\V[1] years old'
  const width3 = calculateTextWidth(text3, true)
  t.is(width3, 17)

  const text4 = 'You owe me 10\\G' // Replaces \G with JPY
  const width4 = calculateTextWidth(text4, true)
  t.is(width4, 16)
})

test('calculateTextWidth - Ignores message codes that do not add text', t => {
  const text = 'Hello my name is \\{large text\\}\\C[3]\\|\\<\\>\\^\\!\\.'
  const width = calculateTextWidth(text, true)
  t.is(width, 27)

  const text2 = 'Hello my name is \\FS[1]'
  const width2 = calculateTextWidth(text2, true)
  t.is(width2, 17)
})
