import test from 'ava'
import { getMultiLineTag, loadEventComments, loadJSON } from '../../src'

test('multiline notetag retrieval', async t => {
  // Before we can retrieve comment tags we need to assign $dataMap
  window.$dataMap = await loadJSON('./tests/helpers/Map001.json')
  const comments = loadEventComments()

  /**
   * Comments are structured as an object by eventID so we need to loop comments
   */
  Object.keys(comments).forEach(eventId => {
    const multiTag = getMultiLineTag(comments[eventId], 'multitag')
    t.deepEqual(multiTag, ['prop: value,cool: yes'], 'Must return array with string value')
  })
})
