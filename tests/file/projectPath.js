import test from 'ava'
import projectPath from '../../src/File/projectPath'

test('projectPath', t => {
  // Returns 'blank' because of mock DOM
  t.is(projectPath(), 'blank')
})
