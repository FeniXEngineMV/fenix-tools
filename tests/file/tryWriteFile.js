import test from 'ava'
import { tryWriteFile } from '../../src/index'
import { base64 } from '../helpers/base64Image'

test('tryWriteFile - test write a string to a file', async t => {
  await t.notThrows(() => {
    tryWriteFile('./tests/dumps/myNewWrittenFile.md', '# Chapter 1 - More To The Story')
  })
})

test('tryWriteFile - save an image with tryWriteFile', async t => {
  tryWriteFile('./tests/dumps/savedImage.png', base64, 'base64')
})
