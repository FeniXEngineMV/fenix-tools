import test from 'ava'
import { hasWebAccess } from '../../src/index'

test('hasWebAccess - test for checking connectivity', async t => {
  const webAccess = await hasWebAccess('https://httpstat.us/200')
  const noAccess = await hasWebAccess('https://httpstat.us/500')
  t.is(webAccess, true, 'You have web access')
  t.is(noAccess, false)
})
